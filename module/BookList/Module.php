<?php
namespace BookList; 

class Module {
    public function getAutoloaderConfig(){
        return [
            'Zend\Loader\ClassMapAutoLoader' => [ __DIR__ . '/autoload_classmap.php'], // Linking autoload_classmap
            'Zend\Loader\StandardAutoLoader' => ['namespaces' => [__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__]]
        ];
    }
    
    public function getConfig(){
        return include __DIR__ . '/config/module.config.php';
    }
}