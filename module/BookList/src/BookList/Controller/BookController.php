<?php

namespace BookList\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use BookList\Form\BookForm;

class BookController extends AbstractActionController {
    
    public function indexAction() {
        return new ViewModel(['books'=>[]]);
    }
    
    public function addAction(){
        $form = new BookForm();
        $form->get('submit')->setValue('Add');
        
        if ($this->getRequest()->isPost()){
            
        }
        return ['form'=> $form];
    }
    
    public function editAction(){
        $form = new BookForm();
        //$form->bind($book);
        $form->get('submit')->setAttribute('value', 'Edit');
        
        $request = $this->request;
        if ($request->isPost()){
            
        }
        
        return ['form' => $form, 'id' => $this->params()->fromRoute('id')];
    }
    
    public function deleteAction(){
        $id = $this->params()->fromRoute('id', 0);
        if (!$id){
            return $this->redirect()->toRoute ('book');
        }
        
        if ($this->request->isPost()){
            
        }
        
        $book = new \stdClass();
        $book->id = $id;
        $book->title = 'Java is Better';
        
        return ['id' => $id, 'book' => $book];
    }
}