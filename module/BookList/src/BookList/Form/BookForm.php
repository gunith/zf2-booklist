<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BookList\Form;

use \Zend\Form\Form;
/**
 * Description of BookForm
 *
 * @author Gunith <gunith@gmail.com>
 */
class BookForm extends Form {
    public function __construct($name = null, $options = array()) {
        parent::__construct('book', $options);
        
        $this->add(['name'=> 'id', 'type' => 'Hidden']);
        $this->add(['name'=>'title', 'type' => 'Text', 'options' => ['label' => 'Title']]);
        $this->add(['name'=> 'author', 'type'=> 'Text', 'options' => ['label' => 'Author']]);
        $this->add(['name'=>'submit', 'type' => 'Submit', 'attributes' => ['value' => 'Go', 'id' => 'submitbutton']]);
    }
}
